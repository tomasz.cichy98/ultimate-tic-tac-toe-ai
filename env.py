import numpy as np

COLS_OUT = 3
ROWS_OUT = 3
COLS_IN = 3
ROWS_IN = 3

class Env:
    def __init__(self):
        self.board_out = np.zeros((ROWS_OUT, COLS_OUT))
        self.boards_in = []
        self.winners_in = []

        # +---+---+---+
        # | 0 | 1 | 2 |
        # +---+---+---+
        # | 3 | 4 | 5 |
        # +---+---+---+
        # | 6 | 7 | 8 |
        # +---+---+---+

        for _ in range(ROWS_OUT*COLS_OUT):
            self.boards_in.append(np.zeros((ROWS_IN, COLS_IN)))
            self.winners_in.append(None)
        
        self.o = -1
        self.x = 1
        self.winner = None
        self.ended = False
        self.num_of_states = 3**(ROWS_OUT*ROWS_IN*COLS_OUT*COLS_IN)

    def is_empty(self, board, i, j):
        return self.boards_in[board][i, j] == 0

    def reward(self, player):
        """
        IN: player (-1, 1)
        OUT: reward 1 for winning -1 for losing
        for some AI models
        """
        # reward only for winning
        if not self.game_over():
            return 0 
        return 1 if self.winner == player else -1

    def game_over(self, board, is_inner_board = True):
        """
        IN: board to check, outer board or one of the inner boards
            is_inner_board
        OUT: True if the board is over, False if not

        check whether the board is finalized.
        set corresponding element of winners_in[] to the winner

        if checking outer board:
            set self.winner to the winner
            set self.ended to True
        """
        pass

    def winners_in_2_board_out(self):
        """
        IN:
        OUT: np.array() representing the outer board
        Convert information about the winner of individual
        small boards to a outer board
        """
        pass
        for i in range(ROWS_OUT):
            for j in range(COLS_OUT):
                idx = ROWS_OUT*i + j
                self.board_out[i,j] = self.winners_in[idx]

    def flatten_board(self):
        """
        IN:
        OUT: long list shape(COLS_OUT*ROWS_OUT*COLS_IN*ROWS_IN, )
        Convert a set of *9* small boards so it is a long list going in layers from top to bottom of the board

        ------------------------------
        | 1  0  2 | 0  3  0 | 0  0  4 |
        | 0  5  0 | 0  6  0 | 0  0  0 |
        | 0  0  0 | 0  0  0 | 0  0  0 |
        ------------------------------
        | 0  0  0 | 0  0  0 | 0  0  0 |
        | 0  0  0 | 0  0  0 | 0  0  0 |
        | 0  0  0 | 0  0  0 | 0  0  0 |
        ------------------------------
        | 0  0  0 | 0  0  0 | 0  0  0 |
        | 0  0  0 | 0  0  0 | 0  0  0 |
        | 0  0  0 | 0  0  0 | 0  0  0 |
        ------------------------------

        --->

        [1, 0, 2, 0, 3, 0, 0, 0, 4, 0, 5, 0, 0, 6, 0, ....]

        """
        # NOTE: might not be optimal
        # but now it works
        
        out = []

        for i_out in range(ROWS_OUT):
            for i in range(ROWS_IN):
                for j_out in range(COLS_OUT):
                    board_index = ROWS_OUT*i_out + j_out
                    out.append(self.boards_in[board_index][i, :])
            
        return np.array(out).flatten()

    def draw_board(self):
        # outer board
        # convert entire the board to a numpy array and the flatten 
        # flat = np.array(self.boards_in.flatten()).flatten()

        flat = self.flatten_board()
        for i in range(len(flat)):
            if i%(COLS_OUT*COLS_IN) == 0:
                # break line
                if i!= 0:
                    print("|", end = "")
                print()

            if i%(ROWS_IN*COLS_OUT*COLS_IN) == 0:
                # horizontal separators
                print("----------"*COLS_OUT)

            if i%COLS_IN == 0:
                # inner board separators
                print("|", end = "")

            print(f" { int(flat[i]) } ", end = "")
        print("|")
        print("----------"*COLS_OUT) 

        # print outer board only
        # TODO: there is a difference between draw and a inner board not finished
        self.winners_in_2_board_out()
        for i in range(ROWS_OUT):
            print("-------------")
            for j in range(COLS_OUT):
                print("  ", end = "")
                if self.board_out[i,j] == self.x:
                    print("x ", end="")
                elif self.board_out[i,j] == self.o:
                     print("o ", end="")
                elif self.board_out[i, j] == 0:
                    print("0 ", end="")
                else:
                    print("  ", end="")
            print("")
        print("-------------")
