# Ultimate Tic Tac Toe AI
Open source Python implementation of Ultimate Tic Tac Toe.

## Road map
1. Create a game in python
1. Build and train AI
1. Interact with http://bejofo.net/ttt to play games vs other people online.
    - web scraping?
    - sending requests?


## References
1. DeepMind's *"Human-level control through deep reinforcement learning"*: https://www.nature.com/articles/nature14236
1. ASCI table generator: https://ozh.github.io/ascii-tables/
1. GitLab project icon by: By Ofek Gila - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=54095462
